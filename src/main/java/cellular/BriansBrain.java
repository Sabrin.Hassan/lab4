package cellular;

import java.util.Random;

import datastructure.CellGrid;
import datastructure.IGrid;



public class BriansBrain implements CellAutomaton {

    IGrid currentGeneration;

    public BriansBrain(int rows, int columns) {
        currentGeneration = new CellGrid(rows, columns, CellState.DEAD);
        initializeCells();

    }

    @Override
    public CellState getCellState(int row, int column) {
        // TODO Auto-generated method stub
        return currentGeneration.get(row, column);
    }

    @Override
    public void initializeCells() {
        // TODO Auto-generated method stub
        Random random = new Random();
		for (int row = 0; row < currentGeneration.numRows(); row++) {
			for (int col = 0; col < currentGeneration.numColumns(); col++) {
				if (random.nextBoolean()) {
					currentGeneration.set(row, col, CellState.ALIVE);
				} else{
					currentGeneration.set(row, col, CellState.DEAD);
				}
			}
		}
        
        
    }

    @Override
    public void step() {
        // TODO Auto-generated method stub
        IGrid nextGeneration = currentGeneration.copy();
        for(int row = 0; row < numberOfRows(); row++){
			for(int column = 0; column < numberOfColumns(); column++){
				currentGeneration.set(row,column,getNextCell(row, column));

			}
		}
		currentGeneration = nextGeneration;
    }

    @Override
    public CellState getNextCell(int row, int col) {
        // TODO Auto-generated method stub

        int aliveNeighbors = countNeighbors(row, col);
        
        if(getCellState(row, col) == CellState.ALIVE){
            return CellState.DYING;
        }
        else if (getCellState(row, col) == CellState.DYING){
            return CellState.DEAD;
        }
        else {
            if(aliveNeighbors == 2){
                return CellState.ALIVE;
            }
            else{
                return CellState.DEAD;
            }
        }
    }

    private int countNeighbors(int row, int col) {
        int[] xOffset = {-1, -1, -1, 0, 0, 1, 1, 1};
        int[] yOffset = {-1, 0, 1, -1, 1, -1, 0, 1};
        int numNeighbours = 0;
        for (int i = 0; i < 8; i++) {
            int neighborX = row + xOffset[i];
            int neighborY = col + yOffset[i];
            if (neighborX < 0 || neighborX >= currentGeneration.numRows() ||
                    neighborY < 0 || neighborY >= currentGeneration.numColumns()) {
                continue;
            }
            if (currentGeneration.get(neighborX, neighborY) == CellState.ALIVE) {
                numNeighbours++;
            }
        }
        return numNeighbours;
    }




    /** 

    private int countNeighbors(int row, int col, CellState state) {
        // TODO
		int numNeighbours = 0; 
		for(int r = row-1; r <= row+1; r++){
			for(int c = col-1; c <= col +1; c++){
				if(r < 0 || c < 0){
					continue;
				}
				if(r >= currentGeneration.numRows() || c >= currentGeneration.numColumns()){
					continue; 
				}
				if (r == row && c == col){
					continue; 
				}
				else if(state == currentGeneration.get(r,c)){
					numNeighbours++;
				}
			}
		}
		return numNeighbours;
    }
    */

    @Override
    public int numberOfRows() {
        // TODO Auto-generated method stub
        return  currentGeneration.numRows();
    }

    @Override
    public int numberOfColumns() {
        // TODO Auto-generated method stub
        return currentGeneration.numColumns();
    }

    @Override
    public IGrid getGrid() {
        // TODO Auto-generated method stub
        return currentGeneration;
    }

}
